import './App.css';
import React from 'react';

function App() {
  const title = React.createElement(
    'h1',
    { style: { color: '#999', fontSize: '19px' } },
    'Solar system planets'
  );

  const planets = [
    'Mercury',
    'Venus',
    'Earth',
    'Mars',
    'Jupiter',
    'Saturn',
    'Uranus',
    'Neptune'
  ];

  const planetList = (
    <ul className="planets-list">
      {planets.map((planet, index) => (
        <li key={index}>{planet}</li>
      ))}
    </ul>
  );

  return (
    <div>
      {title}
      {planetList}
    </div>
  );
}

export default App;
